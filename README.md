# Docker for shopify
## Description
This is dockerization of shopify based on `ubuntu:latest` docker image.

## Why?
Reason for this, is to avoid pollution of host machine with rvm, ruby, shopify and it's dependencies.

## Requirement
- Internet
- Docker
- Docker Compose(recommended, optional)

## Instruction
1. Install Docker,
2. Change default volume folder in docker-compose to the one of your project(or with command without docker-compose),
3. Build image and run, `docker-compose up --build -d`, `docker-compose build; docker-compose up -d`,
4. Enter running Docker, `docker-compose exec shopify /bin/bash`,
5. Enter `shopify login --store name_of_the_shop.myshopify.com` to start login process,
6. Copy link outputed in the cli to browser on the host machine,
7. Login,
8. Wait for cli to be clear,
9. Start shopify server on host '0.0.0.0', `shopify theme serve --host 0.0.0.0`,
10. Start development as normal.


### Where to change default voluem:
- `docker-compose.yml`
```
	volumes:
	      - ../folder_to_your_project:/home/rubyuser/project
```
### more information
- user
	+ username: rubyuser
	+ password: pass
	+ is sudo: yes
	+ shell: bash
	+ $HOME: /home/rubyuser
- rubies
	+ ruby-2.7.2
	+ ruby-3.0.0, default, current
- exposed ports:
	+ 9292
	+ 3456